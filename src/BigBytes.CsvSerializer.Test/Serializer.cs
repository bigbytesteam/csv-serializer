﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigBytes.CsvSerializer.Test
{
    [TestClass]
    public class Serializer
    {
        [TestMethod]
        public void TestNull()
        {
            BigBytes.CsvSerializer.Serializer serializer = new CsvSerializer.Serializer();
            Assert.AreEqual("", serializer.Serialize(null));
            Assert.IsNotNull(serializer.Deserialize(null));
        }
    }
}
