﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BigBytes.CsvSerializer
{
    public class Settings
    {
        /// <summary>
        /// Field separator characters (comma, semicolon, etc.).
        /// </summary>
        public char[] Separator { get; set; }

        /// <summary>
        /// Text enclosure characters (quotation marks, apostrophes, etc.).
        /// </summary>
        public char[] Enclosure { get; set; }

        /// <summary>
        /// Strip field value if it was quoted.
        /// </summary>
        public bool Strip { get; set; }

        /// <summary>
        /// Allow whitespace before value if not quoted.
        /// </summary>
        public bool Whitespace { get; set; }

        /// <summary>
        /// Allow usage of equals sign (="01")
        /// </summary>
        public bool EqualSign { get; set; }

        /// <summary>
        /// Glue separators together (not yet supported).
        /// </summary>
        public bool Glue { get; set; }

        /// <summary>
        /// Use first line as header.
        /// </summary>
        public bool Header { get; set; }
    }
}
